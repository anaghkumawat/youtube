import React from "react"
import Button from "./button"

const list = ["All","Gaming","Songs","Live","Soccer","Cricket","Cooking","Basketball","Tennis"];

const ButtonList = () => {
    return (
        <div className="flex justify-center">
            {list.map((item,index) => {
                return (
                    <Button name={item} key={index}/>
                )
            }
            )}
        </div>
    )
}

export default ButtonList