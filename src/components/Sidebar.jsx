import React from "react"
import { useSelector } from "react-redux";
import Home from "../assets/Home.svg";
import Shorts from "../assets/Shorts.svg";
import Subscriptions from "../assets/Subscriptions.png"
import { Link } from "react-router-dom";

const Sidebar = () => {
const isMenuOpen = useSelector((store) => store.app.isMenuOpen);

    if(!isMenuOpen) return null;

    return (
        <div className="p-3 basis-96">
            <ul>
                <li>
                    <Link to="/">
                        <div className="flex h-10 cursor-pointer items-center px-3 hover:bg-[rgba(0,0,0,0.05)]">
                    <img className="w-6 h-6 mr-6" alt = "Home icon" src = {Home}/>
                    <span>Home</span>
                    </div>
                    </Link>
                </li>
                <li className="flex h-10 cursor-pointer items-center px-3 hover:bg-[rgba(0,0,0,0.05)]">
                    <img className="w-6 h-6 mr-6" alt = "Shorts icon" src = {Shorts}/>
                    <span>Shorts</span>
                </li>
                <li className="flex h-10 cursor-pointer items-center px-3 hover:bg-[rgba(0,0,0,0.05)]">
                    <img className="w-6 h-6 mr-6" alt = "Subscriptions icon" src = {Subscriptions}/>
                    <span className="overflow-hidden">Subscriptions</span>
                </li>
            </ul>
            <h1 className="font-bold pt-5">Subscriptions</h1>
            <ul>
                <li>Music</li>
                <li>Sports</li>
                <li>Gaming</li>
                <li>Movies</li>
            </ul>
            <h1 className="font-bold pt-5">Watch Later</h1>
            <ul>
                <li>Music</li>
                <li>Sports</li>
                <li>Gaming</li>
                <li>Movies</li>
            </ul>
        </div>
    )
}

export default Sidebar