const VideoCard = (props) => {
    return (
        <div className="shadow-lg w-[250px] mx-2 mb-10">
            <img className="rounded-lg" alt="thumbnail" src={props.src}/>
            <h3 className="font-bold py-2 overflow-hidden whitespace-nowrap">{props.title}</h3>
            <div className="grid">
            <span>{props.channelTitle}</span>
            <span>{props.viewCount} views</span>
            </div>
        </div>
    )
}

export default VideoCard