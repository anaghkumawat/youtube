import axios from "axios"
import React, { useEffect,useState } from "react"
import { Link } from "react-router-dom";
import {YOUTUBE_VIDEOS_API} from "../utils/constants"
import VideoCard from "./VideoCard";

const VideoContainer = () => {

    const [videos, setVideos] = useState([]);


    useEffect(() => {
        axios.get(YOUTUBE_VIDEOS_API)
        .then((res) => {
            setVideos(res.data.items)})
    },[])


    return (
        <div className="flex flex-wrap pt-6 justify-between mx-4">{videos.map((video) => {
        return (
        <Link key={video.id} to={"/watch?v="+ video.id}><VideoCard src={video.snippet.thumbnails.medium.url}
        title={video.snippet.title}
        channelTitle={video.snippet.channelTitle}
        viewCount={video.statistics.viewCount}
        /></Link>
        )
        }
    )}
        </div>
    )
}

export default VideoContainer